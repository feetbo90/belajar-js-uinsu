function setup() {
    if (6 % 2 === 0) {
        // console.log("benar")
    } else {
        // console.log("salah")
    }

    // contoh 2
    let bil1 = 5
    let bil2 = 10
    let bil3 = 2
    let bil4 = 11

    console.log("==================================")

    if (bil1 > bil3 && bil2 < bil4) {
        console.log("benar") // satu statement
    } else {
        console.log("salah")
    }

    // struktur keputusan bertingkat (nested if)
    // if (true) {
    //
    // } else if(true) {
    //
    // } else if (false) {
    //
    // } else {
    //
    // }

    // ternary operator
    // sintaks ( logika bolean true) ? benar eksekusi : salah eksekusi
    // False
    (bil4 > 10) ? console.log("benar ternary operator") : console.log("salah ternary operator")

    console.log("==================================")
    // struktur keputusan bertingkat (nested if)

    let nilai = 60
    /***
     * A = nilai > 90
     * B = nilai > 80 && nilai <= 90
     * C = nilai > 70 && nilai <= 80
     * D = nilai <= 70
     */

    if (nilai > 90) {
        console.log("A")
    } else if(nilai > 80 && nilai <= 90) {
        console.log("B")
    } else if (nilai > 70 && nilai <= 80) {
        console.log("C")
    } else {
        console.log("D")
    }

}


function draw() {

    // console.log("halo 2")

}

/***
 *
 * AND OR NOT XOR
 * tipe data boolean
 * && (AND)     OR (||)
 *
 * operator boolean
 * AND
 * T    T   T
 * T    F   F
 * F    T   F
 * F    F   F
 *
 * OR
 * T    T   T
 * T    F   T
 * F    T   T
 * F    F   F
 *
 * NOT
 * T    !T  F
 *
 * operator perbandingan
 * >, <, ===, ==, !=, >=, <=
 *
 * struktur keputusan
 * if (T)
 *
 * if (6 % 2 == 0) {
 *     console.log("benar")
 * } else {
 *     console.log("salah")
 * }
 *
 * let bil1 = 5
 * let bil2 = 10
 * let bil3 = 2
 * let bil4 = 4
 *
 * if (bil1 > bil3 && bil2 < bil4)
 * {
 *     console.log("benar")
 * } else {
 *     console.log("salah")
 * }
 *
 */
