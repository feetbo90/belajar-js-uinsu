function setup() {

    let bilangan1 = 20
    let bilangan2 = 30

    aritmatika(bilangan1, bilangan2)

    console.log("======================================")

    let bilangan3 = 40
    let bilangan4 = 50

    aritmatika(bilangan3, bilangan4)

    // arrow function
    const bagi = (bilangan1, bilangan2) => { console.log( bilangan1 / bilangan2)}

    bagi(2,3)
    // console.log(`bagi : ${bagi}`)


    //
    console.log("modulo")
    let sisaBagi = modulo(5,2)
    console.log(`sisa bagi : ${sisaBagi}`)

    // arrow function
    const sisaBagiReturn = (bilangan, bilangan2) => bilangan % bilangan2
    let sisaBagi2 = sisaBagiReturn(10,3)
    console.log(`sisa bagi 2 : ${sisaBagi2}`)
}

// function bagi(bilangan1, bilangan2) {
//     console.log(`pembagian : ${bagi}`)
// }

// return value
function modulo(parameter1, parameter2) {
    return parameter1 % parameter2
}


// function void
function aritmatika(parameter1, parameter2) {
    // penjumlahan
    let jumlah = parameter1 + parameter2
    // pengurangan
    let kurang = parameter1 - parameter2
    // perkalian
    let kali = parameter1 * parameter2

    console.log(`penjumlahan : ${jumlah}`)
    console.log(`pengurangan : ${kurang}`)
    console.log(`perkalian : ${kali}`)

}

/***
 *
 * nilai awal, batas (mengakhiri perulangan), step
 *
 */
